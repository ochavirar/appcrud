export interface Task {
    id?:string;
    task: string;
    priority: number;
    Owner: string;
    Place: string;
}
