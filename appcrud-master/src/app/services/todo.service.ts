import { Injectable } from '@angular/core';
///////
import {AngularFirestore, AngularFirestoreCollection} from 'angularfire2/firestore';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';
import {Task} from '../models/task';
///////

@Injectable({
  providedIn: 'root'
})
export class TodoService { 
  /////
  private todosCollection : AngularFirestoreCollection<Task>;
  private todos : Observable<Task[]>;
  /////
  constructor(
    //////
    db: AngularFirestore
    //////
  ) {
    /////
    this.todosCollection = db.collection<Task>('todos');
    this.todos = this.todosCollection.snapshotChanges().pipe(map(
      actions => {
        return actions.map( a => {
          const data = a.payload.doc.data();
          const id = a.payload.doc.id;
          return {id, ...data}
        });
      }
    ))
    /////
  }
  /////
  getTodos() {
    return this.todos;
  }
  getTodo(id:string) {
    return this.todosCollection.doc<Task>(id).valueChanges();
  }
  updateTodo(todo: Task, id: string) {
    return this.todosCollection.doc(id).update(todo);
  }
  addTodo(todo: Task) {
    return this.todosCollection.add(todo);
  }
  removeTodo(id: string) {
    return this.todosCollection.doc(id).delete();
  }
  /////
}
